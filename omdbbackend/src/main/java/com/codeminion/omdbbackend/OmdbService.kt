package com.codeminion.omdbbackend

import com.codeminion.omdbbackend.model.MovieFullInfo
import com.codeminion.omdbbackend.model.MovieSearchResult
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface OmdbService {

    /**
     * Request from the back end a set of records matching
     */
    @GET("/")
    fun findMoviesByTitle(@Query("apiKey") apiKey:String, @Query("s") searchStr:String,
                   @Query("page") page:Long = 1):Call<MovieSearchResult>

    /**
     * Request all the information for a specific show or movie.
     */
    @GET("/")
    fun findMovieById(@Query("apiKey") apiKey:String, @Query("i") movieId:String):Call<MovieFullInfo>
}