package com.codeminion.omdbbackend

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Helper callback for retrofit request.
 */
class RetrofitCallback <T> (private val onResponse:((call: Call<T>, response:Response<T>)->Unit)? = null,
                            private val onFailure: ((call: Call<T>, t:Throwable)->Unit)? = null):Callback<T> {
    override fun onFailure(call: Call<T>, t: Throwable) {
        onFailure?.let {
            it(call, t)
        }
    }

    override fun onResponse(call: Call<T>, response: Response<T>) {
        onResponse?.let {
            it(call, response)
        }
    }

}