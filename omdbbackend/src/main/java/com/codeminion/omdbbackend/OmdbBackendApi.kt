package com.codeminion.omdbbackend

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

object OmdbBackendApi {

    private const val CLIENT_TIMEOUT_READ: Long = 120
    private const val CLIENT_TIMEOUT_CONNECTION: Long = 120
    private const val ENDPOINT_URL: String = "https://www.omdbapi.com/"

    private lateinit var mClient: Retrofit
    private lateinit var mOmdbService: OmdbService

    /**
     * Initialize the client only if none exits to ensure
     * we don't perform the heave initialization every time.
     */
    private fun getClient(): Retrofit {
        if (!::mClient.isInitialized) {

            val okHttpClient = OkHttpClient.Builder()
                .readTimeout(CLIENT_TIMEOUT_READ, TimeUnit.SECONDS)
                .connectTimeout(CLIENT_TIMEOUT_CONNECTION, TimeUnit.SECONDS)
                .build()

            val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

            mClient = Retrofit.Builder()
                .baseUrl(ENDPOINT_URL)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .client(okHttpClient)
                .build()
        }

        return mClient
    }

    /**
     * Obtain a reference to the backend service to be
     * able to make requests.
     */
    fun getOmdbService(): OmdbService {
        if (!::mOmdbService.isInitialized) {
            mOmdbService = getClient().create(OmdbService::class.java)
        }

        return mOmdbService
    }


}