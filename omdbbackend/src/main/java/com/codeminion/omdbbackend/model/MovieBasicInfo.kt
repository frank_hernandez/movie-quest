package com.codeminion.omdbbackend.model

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.Json

/**
 * Basic information model containing some
 * of the information provided by the backend.
 */
data class MovieBasicInfo(
    @Json(name = "Title") val title:String,
    @Json(name = "Year") val year:String,
    @Json(name ="imdbID") val imdbID:String,
    @Json(name ="Type") val type:String,
    @Json(name = "Poster") val posterUrlStr:String
):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()?:"",
        parcel.readString()?:"",
        parcel.readString()?:"",
        parcel.readString()?:"",
        parcel.readString()?:""
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(year)
        parcel.writeString(imdbID)
        parcel.writeString(type)
        parcel.writeString(posterUrlStr)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MovieBasicInfo> {
        override fun createFromParcel(parcel: Parcel): MovieBasicInfo {
            return MovieBasicInfo(parcel)
        }

        override fun newArray(size: Int): Array<MovieBasicInfo?> {
            return arrayOfNulls(size)
        }
    }
}