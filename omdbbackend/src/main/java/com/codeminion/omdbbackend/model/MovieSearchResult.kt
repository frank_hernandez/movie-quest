package com.codeminion.omdbbackend.model

import com.squareup.moshi.Json

/**
 * Search results model.
 * Contains the information provided by the back end
 * about a given query.
 *
 * Note: isResponse will be false if there is a problem with the
 * request. In the event of a problem with the request the reason
 * can be obtained from the errorMsg.
 */
data class MovieSearchResult(
    @Json(name = "Search") val searchResults: List<MovieBasicInfo> = listOf(),
    @Json (name = "totalResults") val totalResults: Long = 0,
    @Json (name = "Response") private val isResponse: String,
    @Json(name = "Error") val errorMsg:String = ""
) {
    fun isResponse():Boolean = isResponse == "True"
}