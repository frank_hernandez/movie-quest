package com.codeminion.omdbbackend.model

import com.squareup.moshi.Json

data class MovieFullInfo (@Json(name= "Title") val title:String,
                          @Json(name = "Year") val year:String,
                          @Json(name ="Rated") val rating:String,
                          @Json(name = "Genre") val genre:String,
                          @Json(name = "Actors") val actors:String,
                          @Json (name = "Plot") val  plot:String,
                          @Json (name = "Awards") val  awards:String,
                          @Json (name = "Response") private val isResponse: String,
                          @Json(name = "Error") val errorMsg:String = ""
) {
    fun isResponse():Boolean = isResponse == "True"
}