package com.codeminion.mquitestframework.uiobject

import androidx.test.uiautomator.UiObject
import androidx.test.uiautomator.UiSelector
import com.codeminion.mquitestframework.UiMovieDetailsAdapter
import com.codeminion.mquitestframework.UiUtils

class UiMovieDetailsAdapterImpl(
    private val appPackage: String,
    private val elementId: String,
    private val selector: UiObject
) : UiMovieDetailsAdapter {

    companion object {
        private const val TITLE_ID = "movieTitle"
        private const val YEAR_ID = "movieYear"
        private const val PLOT_ID = "moviePlot"
        private const val ACTORS_ID = "movieActors"
        private const val ACTOR_TEXT_ID = "actorTitle"
    }

    override fun getTitle(): String {
        return selector.getChild(
            UiSelector().resourceId(
                UiUtils.generateElementId(
                    appPackage,
                    TITLE_ID
                )
            )
        ).text
    }

    override fun getPlot(): String {
        return selector.getChild(
            UiSelector().resourceId(
                UiUtils.generateElementId(
                    appPackage,
                    PLOT_ID
                )
            )
        ).text
    }

    override fun getActorCount(): Int {
        val elementId = UiUtils.generateElementId(
            appPackage,
            ACTORS_ID
        )
        return UiUtils.scrollUiToElement(
            selector.getChild(
                UiSelector().resourceId(
                    elementId
                )
            )
        ).getChild(
            UiSelector().resourceId(
                elementId
            )
        ).childCount
    }

    override fun getActor(position: Int): String {
        val elementId = UiUtils.generateElementId(
            appPackage,
            ACTORS_ID
        )

        return UiUtils.scrollUiToElement(
            selector.getChild(
                UiSelector().resourceId(
                    elementId
                )
            )
        ).getChild(
            UiSelector().resourceId(
                elementId
            )
        ).getChild(UiSelector().index(position)).text
    }

    override fun getUiObject(): UiObject {
        return selector
    }


}