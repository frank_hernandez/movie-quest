package com.codeminion.mquitestframework.uiobject

import android.util.Log
import androidx.test.uiautomator.UiObject
import androidx.test.uiautomator.UiSelector
import com.codeminion.mquitestframework.UiMovieItemAdapter
import com.codeminion.mquitestframework.UiUtils

class UiMovieItemAdapterImpl(
    private val appPackage: String,
    private val elementId: String,
    private val selector: UiObject
) : UiMovieItemAdapter {

    companion object {
        private const val TITLE_ID = "movieTitle"
        private const val YEAR_ID = "movieYear"
    }

    override fun getTitle(): String {
        return selector.getChild(
            UiSelector().resourceId(
                UiUtils.generateElementId(
                    appPackage,
                    TITLE_ID
                )
            )
        ).text
    }

    override fun getYear(): String {
        return selector.getChild(
            UiSelector().resourceId(
                UiUtils.generateElementId(
                    appPackage,
                    YEAR_ID
                )
            )
        ).text
    }

    override fun getUiObject(): UiObject {
        return selector
    }

}