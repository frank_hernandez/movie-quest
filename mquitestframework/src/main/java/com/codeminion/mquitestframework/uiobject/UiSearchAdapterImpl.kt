package com.codeminion.mquitestframework.uiobject

import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiObject
import androidx.test.uiautomator.UiSelector
import com.codeminion.mquitestframework.UiSearchAdapter
import com.codeminion.mquitestframework.UiUtils

class UiSearchAdapterImpl(
    private val device: UiDevice,
    private val appPackage: String,
    private val elementId: String,
    private val selector: UiObject
) : UiSearchAdapter {

    companion object {
        private const val TIMEOUT: Long = 1000
        private const val SEARCH_FIELD_ID = "search_src_text"
    }

    override fun performSearch(query: String) {
        selector.click()

        val searchEntry =
            device.findObject(
                UiSelector()
                    .resourceId(
                        UiUtils.generateElementId(appPackage,
                            SEARCH_FIELD_ID
                        ))
            )
        searchEntry.waitForExists(TIMEOUT)

        searchEntry.text = query
        device.pressEnter()

    }

    override fun getSearchEntry(): String {
        return device.findObject(
            UiSelector()
                .resourceId(
                    UiUtils.generateElementId(appPackage,
                        SEARCH_FIELD_ID
                    ))
        ).text
    }

    override fun getUiObject(): UiObject {
        return selector
    }

}