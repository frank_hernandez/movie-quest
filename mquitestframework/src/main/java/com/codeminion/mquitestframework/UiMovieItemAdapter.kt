package com.codeminion.mquitestframework

/**
 * Adapter for the Movie Basic Info view
 * Extracts away the internals of the UI to
 * increase robustness of test cases by reducing
 * breakage when internal structure changes.
 */
interface UiMovieItemAdapter: UiAdapter {

    /**
     * Returns the title of the movie item
     */
    fun getTitle():String

    /**
     * Returns the year of the movie item
     */
    fun getYear():String

}