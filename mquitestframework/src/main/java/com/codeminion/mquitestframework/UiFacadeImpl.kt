package com.codeminion.mquitestframework

import android.widget.LinearLayout
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiSelector
import com.codeminion.mquitestframework.uiobject.UiMovieDetailsAdapterImpl
import com.codeminion.mquitestframework.uiobject.UiMovieItemAdapterImpl
import com.codeminion.mquitestframework.uiobject.UiSearchAdapterImpl

class UiFacadeImpl private constructor(
    private val device: UiDevice,
    private val appPackage: String
) : UiFacade {

    companion object {
        fun initialize(device: UiDevice, appPackage: String): UiFacade {
            return UiFacadeImpl(device, appPackage)
        }
    }

    override fun getSearchView(searchItemId: String): UiSearchAdapter {
        val uiObject = device.findObject(
            UiSelector().resourceId(
                UiUtils.generateElementId(
                    appPackage,
                    searchItemId
                )
            )
        )
        return UiSearchAdapterImpl(
            device,
            appPackage,
            searchItemId,
            uiObject
        )
    }

    override fun getMovieItem(holderId: String, position: Int): UiMovieItemAdapter {
        val uiObject = device.findObject(
            UiSelector().resourceId(UiUtils.generateElementId(appPackage, holderId))
        )

        val child = uiObject.getChild(UiSelector().className(LinearLayout::class.java.name).index(position))
        return UiMovieItemAdapterImpl(
            appPackage, holderId, child
        )
    }

    override fun getMovieDetails(holderId: String): UiMovieDetailsAdapter {

        val uiObject = device.findObject(
            UiSelector().resourceId(UiUtils.generateElementId(appPackage, holderId))
        )

        return UiMovieDetailsAdapterImpl(
            appPackage, holderId, uiObject
        )
    }
}