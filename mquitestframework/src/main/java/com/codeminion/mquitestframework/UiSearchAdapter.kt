package com.codeminion.mquitestframework

interface UiSearchAdapter: UiAdapter {

    /**
     * Performs a search with the specified query
     */
    fun performSearch(query:String)

    /**
     * Returns the text in the search field
     */
    fun getSearchEntry():String
}