package com.codeminion.mquitestframework

interface UiFacade {

    /**
     * Returns an abstraction of the search view for
     * testing.
     * @param searchItemId ID of the search menu view
     */
    fun getSearchView(searchItemId: String): UiSearchAdapter

    /**
     * Returns the movie item at the specified position
     * @param holderId ID of the container of the movie items
     * @param position Position of the item to get
     */
    fun getMovieItem(holderId: String, position: Int): UiMovieItemAdapter

    /**
     * Returns a movie details adapter with all the information
     * about a given movie.
     * @param holderId ID of the movie information holder.
     */
    fun getMovieDetails(holderId:String):UiMovieDetailsAdapter

}