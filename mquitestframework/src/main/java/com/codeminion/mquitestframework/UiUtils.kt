package com.codeminion.mquitestframework

import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiObject
import androidx.test.uiautomator.UiScrollable
import androidx.test.uiautomator.UiSelector

object UiUtils {

    private const val MAX_SWIPES = 1000

    /**
     * Helper to find an element even when it exist
     * past the screen limits
     */
    fun scrollUiToElement(uiElement: UiObject): UiScrollable {
        return UiScrollable(UiSelector().scrollable(true)).apply {
            setAsVerticalList()
            maxSearchSwipes = MAX_SWIPES
            scrollIntoView(uiElement)
        }
    }

    /**
     * Helper to find an element even when it exist
     * past the screen limits
     */
    fun scrollUiToElement(uiElement: UiSelector): UiScrollable {
        return UiScrollable(UiSelector().scrollable(true)).apply {
            setAsVerticalList()
            maxSearchSwipes = MAX_SWIPES
            scrollIntoView(uiElement)
        }
    }

    /**
     * Generates the full id of an element.
     */
    fun generateElementId(appPackage: String, elementId: String): String {
        return "$appPackage:id/$elementId"
    }

    /**
     * Searches for the item in the specified scrollable view.
     */
    fun findObject(device: UiDevice, appPackage: String, elementId: String): UiObject {
        val componentId: String = generateElementId(appPackage, elementId)

        val uiSelector = device.findObject(UiSelector().resourceId(componentId))

        scrollUiToElement(uiSelector)

        return uiSelector
    }


}