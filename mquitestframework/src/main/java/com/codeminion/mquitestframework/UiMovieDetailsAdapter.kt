package com.codeminion.mquitestframework

/**
 * Adapter for the Movie Details view.
 * Extracts away the internals of the UI to
 * increase robustness of test cases by reducing
 * breakage when internal structure changes.
 */
interface UiMovieDetailsAdapter: UiAdapter {

    /**
     * Returns the title of the movie details view
     */
    fun getTitle():String

    /**
     * Returns the plot of the movie details view.
     */
    fun getPlot():String

    /**
     * Returns the amount of actors listed.
     */
    fun getActorCount():Int

    /**
     * Returns the actor at the given position
     */
    fun getActor(position:Int):String

}