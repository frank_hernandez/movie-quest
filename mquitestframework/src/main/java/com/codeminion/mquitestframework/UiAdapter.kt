package com.codeminion.mquitestframework

import androidx.test.uiautomator.UiObject

interface UiAdapter {

    /**
     * Returns the UI Object for this adapter.
     */
    fun getUiObject():UiObject
}