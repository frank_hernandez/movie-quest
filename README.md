# Moview Quest
Movie Quest is a lightweight movie query app. It allows the user to query by title and presents a list of results. Clicking on a given item will present the user with some additional information about the selected item. This uses OMDb API so the results are not just limited to movies but include shows and video games as well. 


## What is the module Composition of Movie Quest?

  - **App**: Contains application-specific code.
  - **Omdb Backend**: Contains code for dealing with the OMDB backend and exposes common interfaces for the consumers. 
  - **MQ UI Test Framework**: Contains code that encapsulates the UI and exposes higher level APIs to allow for writting UI tests that are robust and less prone to breaking due to UI modifications.   


## Available in the Google Play Store
   - http://bit.ly/MovieQuestApp


License
----

ISC
