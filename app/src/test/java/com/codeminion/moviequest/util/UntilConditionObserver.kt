package com.codeminion.moviequest.util

import androidx.lifecycle.*

/**
 * Observer helper that will continue to
 * observe until the condition specified in the
 * predicate is met then it will stop observing.
 */
private class UntilConditionObserver<T>(
    private val predicate: (T) -> Boolean,
    private val handler: (T) -> Unit
) : Observer<T>, LifecycleOwner {
    private val lifecycle = LifecycleRegistry(this)

    init {
        lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
    }

    override fun getLifecycle(): Lifecycle = lifecycle

    override fun onChanged(t: T) {
        if (predicate(t)) {
            handler(t)
            lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        }
    }
}

/**
 * Observe until the specify predicate is met
 * and then stop observing.
 */
fun <T> LiveData<T>.observeUntil(predicate: (T) -> Boolean, onChangeHandler: (T) -> Unit) {
    val observer = UntilConditionObserver(
        predicate = predicate,
        handler = onChangeHandler
    )

    observe(observer, observer)
}

/**
 * Observe until the first change
 * then stop observing.
 */
fun <T> LiveData<T>.observeOnce(onChangeHandler: (T) -> Unit) {
    val observer = UntilConditionObserver(
        predicate = { true },
        handler = onChangeHandler
    )

    observe(observer, observer)
}

