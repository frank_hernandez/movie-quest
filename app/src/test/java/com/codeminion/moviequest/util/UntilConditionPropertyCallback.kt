package com.codeminion.moviequest.util

import androidx.databinding.Observable
import androidx.databinding.ObservableField

private class UntilConditionPropertyCallback<T>(
    private val predicate: (T) -> Boolean,
    private val handler: (T) -> Unit
) : Observable.OnPropertyChangedCallback() {

    override fun onPropertyChanged(sender: Observable, propertyId: Int) {

        val senderField = sender as ObservableField<T>

        val senderValue = senderField.get()

        senderValue?.let{
            if (predicate(it)) {
                handler(it)
                sender.removeOnPropertyChangedCallback(this)
            }
        }


    }
}

/**
 * Observe until the predicate is met then
 * stop observing.
 */
fun <T> ObservableField<T>.observeUntil(
    predicate: (T) -> Boolean,
    handler: (T) -> Unit) {

    val callback = UntilConditionPropertyCallback(
        predicate = predicate,
        handler = handler
    )

    this.addOnPropertyChangedCallback(callback)
}

/**
 * Observe until the first change then stop observing
 */
fun <T> ObservableField<T>.observeOnce(
    handler: (T) -> Unit) {

    val callback = UntilConditionPropertyCallback(
        predicate = { true },
        handler = handler
    )

    this.addOnPropertyChangedCallback(callback)
}