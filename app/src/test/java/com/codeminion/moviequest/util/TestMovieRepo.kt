package com.codeminion.moviequest.util

import com.codeminion.moviequest.MovieRepository
import com.codeminion.moviequest.model.ResultPage
import com.codeminion.omdbbackend.model.MovieBasicInfo
import com.codeminion.omdbbackend.model.MovieFullInfo

/**
 * Test Repo Stub
 */
class TestMovieRepo: MovieRepository {

    companion object {
        val TEST_BASIC_MOVIE = MovieBasicInfo(
        title = "Guardians of the Galaxy Vol. 2",
        year = "2017",
        imdbID = "tt3896198",
        type = "movie",
        posterUrlStr = "https://m.media-amazon.com/images/M/MV5BMTg2MzI1MTg3OF5BMl5BanBnXkFtZTgwNTU3NDA2MTI@._V1_SX300.jpg"
        )

        val TEST_FULL_MOVIE = MovieFullInfo(
            title = "Guardians of the Galaxy Vol. 2",
            year = "2017",
            rating = "PG-13",
            actors = "Chris Pratt, Zoe Saldana, Dave Bautista, Vin Diesel",
            awards = "Nominated for 1 Oscar. Another 12 wins & 42 nominations.",
            plot = "The Guardians struggle to keep together as a team while dealing with their personal family issues, notably Star-Lord's encounter with his father the ambitious celestial being Ego.",
            genre = "Action, Adventure, Comedy, Sci-Fi",
            isResponse = "True"
        )

        val TEST_BASIC_RESULT_LIST = listOf(TEST_BASIC_MOVIE)
    }

    override fun findMoviesByTitle(titleStr: String, page: Long): ResultPage<MovieBasicInfo> = ResultPage(TEST_BASIC_RESULT_LIST, 1, 1)

    override fun findMovieById(id: String): MovieFullInfo = TEST_FULL_MOVIE

}