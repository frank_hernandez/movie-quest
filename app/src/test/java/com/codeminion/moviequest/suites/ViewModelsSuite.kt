package com.codeminion.moviequest.suites

import com.codeminion.moviequest.tests.MovieDetailsViewModelTest
import com.codeminion.moviequest.tests.SearchViewModelTest
import org.junit.runner.RunWith
import org.junit.runners.Suite

/**
 * Suite grouping only the standard view model
 * tests. Suites allow
 * for partitioning the test execution as needed.
 */
@RunWith(Suite::class)
@Suite.SuiteClasses(SearchViewModelTest::class,
    MovieDetailsViewModelTest::class)
class ViewModelsSuite