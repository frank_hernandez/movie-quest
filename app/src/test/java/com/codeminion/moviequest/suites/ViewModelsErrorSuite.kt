package com.codeminion.moviequest.suites

import com.codeminion.moviequest.tests.MovieDetailsViewModelErrorTest
import com.codeminion.moviequest.tests.SearchViewModelErrorTest
import org.junit.runner.RunWith
import org.junit.runners.Suite

/**
 * Suite of tests that groups only
 * the Error tests. Suites allow
 * for partitioning the test execution as needed.
 */
@RunWith(Suite::class)
@Suite.SuiteClasses(SearchViewModelErrorTest::class,
                    MovieDetailsViewModelErrorTest::class)
class ViewModelsErrorSuite