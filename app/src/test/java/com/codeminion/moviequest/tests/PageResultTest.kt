package com.codeminion.moviequest.tests

import com.codeminion.moviequest.model.ResultPage
import com.codeminion.moviequest.util.TestMovieRepo
import com.codeminion.omdbbackend.model.MovieBasicInfo
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.not
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class PageResultTest {

    @Before
    fun setup() {
    }

    @Test
    fun testPageResultCount() {
        val toTest:Long = 1
        val page = ResultPage<MovieBasicInfo>(
            results = emptyList(),
            resultTotal = toTest,
            pageNum = 0
        )

        assertThat(page.resultTotal, `is`(toTest))
    }

    @Test
    fun testPageNumber() {
        val toTest:Long = 1
        val page = ResultPage<MovieBasicInfo>(
            results = emptyList(),
            resultTotal = 0,
            pageNum = toTest
        )

        assertThat(page.pageNum, `is`(toTest))

    }

    @Test
    fun testIsEmpty() {
        val toTest:List<MovieBasicInfo> = emptyList()
        val page = ResultPage(
            results = toTest,
            resultTotal = 0,
            pageNum = 0
        )

        assertThat(page.results.size, `is`(toTest.size))
    }

    @Test
    fun testListValue() {
        val toTest:List<MovieBasicInfo> = listOf(
            TestMovieRepo.TEST_BASIC_MOVIE
        )

        val page = ResultPage(
            results = toTest,
            resultTotal = 200,
            pageNum = 1
        )

        assertThat(page.results.size, `is`(not(0)))
        assertThat(page.results[0].title, `is`(TestMovieRepo.TEST_BASIC_MOVIE.title))

    }
}