package com.codeminion.moviequest.tests

import com.codeminion.moviequest.search.viewmodel.SearchViewModel
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.codeminion.moviequest.MovieResource
import com.codeminion.moviequest.RepositoryBridge
import com.codeminion.moviequest.util.TestMovieRepo
import com.codeminion.moviequest.util.observeOnce
import com.codeminion.moviequest.util.observeUntil
import com.codeminion.omdbbackend.model.MovieBasicInfo
import org.hamcrest.Matchers.*
import org.junit.Rule
import org.junit.rules.TestRule
import java.util.concurrent.CompletableFuture

/**
 * Tests to evaluate the states of the View Model
 * for the SearchViewModel
 */
@RunWith(JUnit4::class)
class SearchViewModelTest {

    companion object {
        private const val TEST_TIMEOUT:Long = 2000
        private const val TEST_TITLE = "Guardians"
    }

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    private lateinit var mSearchViewModel: SearchViewModel

    @Before
    fun setUp() {
        RepositoryBridge.setRepositoryImpl(repo = TestMovieRepo())
        mSearchViewModel = SearchViewModel()
    }

    /**
     * Test to validate the selection of a movie.
     */
    @Test(timeout = TEST_TIMEOUT)
    fun testMovieSelection() {

        mSearchViewModel.setSelectedMovie(TestMovieRepo.TEST_BASIC_MOVIE)

        mSearchViewModel.getSelectedMovieData().observeOnce {
            assertThat(it.title, `is`(TestMovieRepo.TEST_BASIC_MOVIE.title))
        }
    }

    /**
     * Test that searching will trigger the model
     * into searching.
     */
    @Test(timeout = TEST_TIMEOUT)
    fun testSearchByTitleTriggerSearching() {

        mSearchViewModel.getSearchModel().observeUntil({
            it.status == MovieResource.SEARCHING
        }) {
            assertThat(it.status, `is`(MovieResource.SEARCHING))
        }

        mSearchViewModel.findMoviesByTitle(TEST_TITLE)

    }

    /**
     * Test to validate that the search completion will
     * trigger the correct state and provide the right data.
     */
    @Test(timeout = TEST_TIMEOUT)
    fun testSearchByTitleResults() {

        val resultFuture: CompletableFuture<MovieResource<List<MovieBasicInfo>>> = CompletableFuture()

        mSearchViewModel.getSearchModel().observeUntil({
            it.status == MovieResource.SEARCH_DONE
        }
        ) {
            resultFuture.complete(it)
        }

        mSearchViewModel.findMoviesByTitle(TEST_TITLE)

        val result = resultFuture.get()
        assertThat(result.status, `is`(MovieResource.SEARCH_DONE))
        assertThat(result.data.size, equalTo(TestMovieRepo.TEST_BASIC_RESULT_LIST.size))
        assertThat(result.data[0], equalTo(TestMovieRepo.TEST_BASIC_RESULT_LIST[0]))
    }

}