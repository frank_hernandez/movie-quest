package com.codeminion.moviequest.tests

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.codeminion.moviequest.MovieResource
import com.codeminion.moviequest.RepositoryBridge
import com.codeminion.moviequest.search.viewmodel.MovieDetailsViewModel
import com.codeminion.moviequest.util.TestMovieRepo
import com.codeminion.moviequest.util.observeOnce
import com.codeminion.moviequest.util.observeUntil
import com.codeminion.omdbbackend.model.MovieFullInfo
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.notNullValue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.util.concurrent.CompletableFuture

/**
 * Tests to evaluate the states of the View Model
 * for the MovieDetailsViewModel
 */
@RunWith(JUnit4::class)
class MovieDetailsViewModelTest {

    companion object {
        private const val TEST_TIMEOUT:Long = 2000
    }

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()
    private lateinit var mMovieDetailsModel: MovieDetailsViewModel

    @Before
    fun setup() {
        RepositoryBridge.setRepositoryImpl(TestMovieRepo())
        mMovieDetailsModel = MovieDetailsViewModel()
    }

    /**
     * Test to validate the updating of the
     * poster URL in the view model.
     */
    @Test(timeout = TEST_TIMEOUT)
    fun testPosterUrlSet() {
        val future: CompletableFuture<String> = CompletableFuture()

        mMovieDetailsModel.getPosterImageUrl().observeOnce {
            future.complete(it)
        }

        mMovieDetailsModel.updatePosterImage(TestMovieRepo.TEST_BASIC_MOVIE.posterUrlStr)

        val url = future.get()

        assertThat(url, notNullValue())
        assertThat(url, `is`(TestMovieRepo.TEST_BASIC_MOVIE.posterUrlStr))

    }

    /**
     * Test to evaluate that a search request
     * for a full movie info will trigger the
     * searching state.
     */
    @Test(timeout = TEST_TIMEOUT)
    fun testFullInfoSearchTriggerSearching() {
        val future: CompletableFuture<MovieResource<MovieFullInfo>> = CompletableFuture()

        mMovieDetailsModel.getMovieInfoData().observeOnce {
            future.complete(it)
        }

        mMovieDetailsModel.queryMovieInfoData(TestMovieRepo.TEST_BASIC_MOVIE.imdbID)

        val result = future.get()
        assertThat(result, notNullValue())
        assertThat(result.status, `is`(MovieResource.SEARCHING))
    }

    /**
     * Evaluate that the search request provides
     * the expected done state and data
     */
    @Test(timeout = TEST_TIMEOUT)
    fun testFullInfoSearchResult() {
        val future: CompletableFuture<MovieResource<MovieFullInfo>> = CompletableFuture()

        mMovieDetailsModel.getMovieInfoData().observeUntil(
            { it.status == MovieResource.SEARCH_DONE }) {
            future.complete(it)
        }

        mMovieDetailsModel.queryMovieInfoData(TestMovieRepo.TEST_BASIC_MOVIE.imdbID)

        val result = future.get()
        assertThat(result, notNullValue())
        assertThat(result.data.title, `is`(TestMovieRepo.TEST_FULL_MOVIE.title))
    }
}