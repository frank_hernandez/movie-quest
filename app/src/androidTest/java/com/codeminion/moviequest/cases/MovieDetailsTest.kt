package com.codeminion.moviequest.cases

import android.content.Intent
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import com.codeminion.mquitestframework.UiFacade
import com.codeminion.mquitestframework.UiFacadeImpl
import com.codeminion.mquitestframework.UiMovieItemAdapter
import com.codeminion.mquitestframework.UiSearchAdapter
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MovieDetailsTest {

    companion object {
        private const val APP_PACKAGE = "com.codeminion.moviequest"
        private const val START_TIMEOUT:Long = 4000
        private const val SHORT_TIMEOUT:Long = 1000
        private const val TITLE_TEST_QUERY = "Guardians"
        private const val TEST_TITLE = "Guardians of the Galaxy Vol. 2"
        private const val TEST_YEAR = "2017"
        private const val TEST_ACTOR = "Zoe Saldana"
        private const val TEST_ACTOR_TOTAL = 4
        private const val TEST_ACTOR_POS = 1
    }

    private lateinit var mDevice:UiDevice

    @Before
    fun startActivity() {

        val myInstrumentation = getInstrumentation()

        mDevice = UiDevice.getInstance(myInstrumentation)

        if(!mDevice.isScreenOn) {
            mDevice.wakeUp()
            // Assumes the test device does not have a lock screen
            mDevice.swipe(mDevice.displayWidth / 2, mDevice.displayHeight - 10, mDevice.displayWidth / 2, 10, 10)
        }

        // Start from the home screen
        mDevice.pressHome()

        // Wait for launcher
        val launcherPackage = mDevice.launcherPackageName
        assertThat(launcherPackage, notNullValue())
        mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), START_TIMEOUT)


        // Launch the app
        val remoteContext = myInstrumentation.targetContext.createPackageContext(APP_PACKAGE, 0)

        val intent = remoteContext.packageManager
            .getLaunchIntentForPackage(APP_PACKAGE)
        // Clear out any previous instances
        intent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        remoteContext.startActivity(intent)


        // Wait for the app to appear
        mDevice.wait(
            Until.hasObject(By.pkg(APP_PACKAGE).depth(0)),
            START_TIMEOUT
        )

        mDevice.wait(Until.hasObject(By.pkg(APP_PACKAGE).depth(0)), START_TIMEOUT)
    }

    /**
     * Selects the first Guardians for the Galaxy Movie
     * and test that the details view is loaded properly
     * by checking that the movie title there matches
     * the expected one.
     */
    @Test
    fun testMovieDetailsGuardiansTitle() {

        val helperFacade: UiFacade = UiFacadeImpl.initialize(mDevice, APP_PACKAGE)

        val search: UiSearchAdapter = helperFacade.getSearchView("movieSearch").apply {
            performSearch(TITLE_TEST_QUERY)
        }

        mDevice.waitForWindowUpdate(APP_PACKAGE, SHORT_TIMEOUT)

        val movieItem: UiMovieItemAdapter = helperFacade.getMovieItem("movieList", 1)

        val movieTitle = TEST_TITLE

        assertThat(movieItem, notNullValue())
        assertThat(movieItem.getTitle(), `is`(movieTitle))
        assertThat(movieItem.getYear(), `is`(TEST_YEAR))

        movieItem.getUiObject().click()

        mDevice.waitForWindowUpdate(APP_PACKAGE, SHORT_TIMEOUT)

        val movieDetails = helperFacade.getMovieDetails("movieDetailsContainer")

        assertThat(movieDetails.getUiObject().exists(), `is`(true))
        assertThat(movieDetails.getTitle(), `is`(movieTitle))

    }

    /**
     * Selects the first Guardians for the Galaxy Movie
     * and test that the details view is loaded properly
     * by checking that the movie actor count matches
     * the expected count
     */
    @Test
    fun testMovieDetailsGuardiansActorCount() {

        val helperFacade: UiFacade = UiFacadeImpl.initialize(mDevice, APP_PACKAGE)

        val search: UiSearchAdapter = helperFacade.getSearchView("movieSearch").apply {
            performSearch(TITLE_TEST_QUERY)
        }

        mDevice.waitForWindowUpdate(APP_PACKAGE, SHORT_TIMEOUT)

        val movieItem: UiMovieItemAdapter = helperFacade.getMovieItem("movieList", 1)

        val movieTitle = TEST_TITLE

        assertThat(movieItem, notNullValue())
        assertThat(movieItem.getTitle(), `is`(movieTitle))
        assertThat(movieItem.getYear(), `is`(TEST_YEAR))

        movieItem.getUiObject().click()

        mDevice.waitForWindowUpdate(APP_PACKAGE, SHORT_TIMEOUT)

        val movieDetails = helperFacade.getMovieDetails("movieDetailsContainer")

        assertThat(movieDetails.getUiObject().exists(), `is`(true))
        assertThat(movieDetails.getActorCount(), `is`(TEST_ACTOR_TOTAL))
        assertThat(movieDetails.getActor(TEST_ACTOR_POS), `is`(TEST_ACTOR))

    }

}