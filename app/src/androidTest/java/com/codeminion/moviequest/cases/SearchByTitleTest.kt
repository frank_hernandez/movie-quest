package com.codeminion.moviequest.cases

import android.app.Instrumentation
import android.content.Intent
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import com.codeminion.mquitestframework.UiFacade
import com.codeminion.mquitestframework.UiFacadeImpl
import com.codeminion.mquitestframework.UiMovieItemAdapter
import com.codeminion.mquitestframework.UiSearchAdapter
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SearchByTitleTest  {

    companion object {
        private const val APP_PACKAGE = "com.codeminion.moviequest"
        private const val MAIN_ACTIVITY = "com.codeminion.moviequest.search.SearchActivity"
        private const val START_TIMEOUT:Long = 4000
        private const val SHORT_TIMEOUT:Long = 1000
        private const val TEST_TITLE_QUERY = "Guardians"
        private const val TEST_TITLE = "Guardians of the Galaxy"
        private const val TEST_YEAR = "2014"

    }

    private lateinit var mDevice:UiDevice

    @Before
    fun startActivity() {

        val myInstrumentation = getInstrumentation()

        mDevice = UiDevice.getInstance(myInstrumentation)

        if(!mDevice.isScreenOn) {
            mDevice.wakeUp()
            // Assumes the test device does not have a lock screen
            mDevice.swipe(mDevice.displayWidth / 2, mDevice.displayHeight - 10, mDevice.displayWidth / 2, 10, 10)
        }

        val monitor: Instrumentation.ActivityMonitor = myInstrumentation.addMonitor(MAIN_ACTIVITY, null, false)

        // Start from the home screen
        mDevice.pressHome()

        // Wait for launcher
        val launcherPackage = mDevice.launcherPackageName
        assertThat(launcherPackage, notNullValue())
        mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), START_TIMEOUT)


        // Launch the app
        val remoteContext = myInstrumentation.targetContext.createPackageContext(APP_PACKAGE, 0)

        val intent = remoteContext.packageManager
            .getLaunchIntentForPackage(APP_PACKAGE)
        // Clear out any previous instances
        intent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        remoteContext.startActivity(intent)


        // Wait for the app to appear
        mDevice.wait(
            Until.hasObject(By.pkg(APP_PACKAGE).depth(0)),
            START_TIMEOUT
        )

        mDevice.wait(Until.hasObject(By.pkg(APP_PACKAGE).depth(0)), START_TIMEOUT)
    }

    /**
     * Test to evaluate the test feature.
     * It will search for a set of movies by title and check that the first item
     * is the expected one.
     */
    @Test
    fun testSearchByTitleValid() {

        val helperFacade: UiFacade = UiFacadeImpl.initialize(mDevice, APP_PACKAGE)

        val search: UiSearchAdapter = helperFacade.getSearchView("movieSearch").apply {
            performSearch(TEST_TITLE_QUERY)
        }

        mDevice.waitForWindowUpdate(APP_PACKAGE, SHORT_TIMEOUT)

        val movieItem: UiMovieItemAdapter = helperFacade.getMovieItem("movieListFragment", 0)

        assertThat(movieItem, notNullValue())

        assertThat(movieItem.getTitle(), `is`(TEST_TITLE))
        assertThat(movieItem.getYear(), `is`(TEST_YEAR))
    }

    /**
     * Test to evaluate and invalid search.
     * It will search and expect no results.
     */
    @Test
    fun testSearchByTitleInvalid() {

        val helperFacade: UiFacade = UiFacadeImpl.initialize(mDevice, APP_PACKAGE)

        val search: UiSearchAdapter = helperFacade.getSearchView("movieSearch").apply {
            performSearch("JGJDJGJGJG")
        }

        mDevice.waitForWindowUpdate(APP_PACKAGE, SHORT_TIMEOUT)

        val movieItem: UiMovieItemAdapter = helperFacade.getMovieItem("movieListFragment", 0)

        assertThat(movieItem.getUiObject().exists(), `is`(false))

    }


}