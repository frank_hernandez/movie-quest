package com.codeminion.moviequest;

import androidx.annotation.IntDef;

/**
 * Resources encapsulate data as well as
 * status. This allows for better support
 * of UI states while performing Async operations.
 * @param <T>
 */
public class MovieResource<T> {

    @IntDef({NONE, SEARCHING, SEARCH_DONE, SEARCH_ERROR})
    @interface Status {}
    public static final int NONE = 0;
    public static final int SEARCHING = 1;
    public static final int SEARCH_DONE = 2;
    public static final int SEARCH_ERROR = 3;

    private T mData;
    private final @Status int mStatus;
    private Throwable mError;

    public MovieResource(@Status int status, T data) {
        mData = data;
        mStatus = status;
    }

    public MovieResource(@Status int status, T data, Throwable error) {
        mStatus = status;
        mError = error;
        mData = data;
    }

    public T getData(){
        return mData;
    }

    public @Status int getStatus(){
        return mStatus;
    }

    public Throwable getError() {
        return mError;
    }

}
