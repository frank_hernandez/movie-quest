package com.codeminion.moviequest

import com.codeminion.moviequest.model.ResultPage
import com.codeminion.omdbbackend.model.MovieBasicInfo
import com.codeminion.omdbbackend.model.MovieFullInfo

/**
 * Interface for all repository layers in the app.
 */
interface MovieRepository {

    /**
     * Returns a list of movies whose title include the titleStr
     * @param titleStr title query
     * @param page page of results to fetch
     */
    fun findMoviesByTitle(titleStr:String, page:Long = 1):ResultPage<MovieBasicInfo>

    /**
     * Returns all the information available for the given
     * show or movie
     */
    fun findMovieById(id:String):MovieFullInfo
}