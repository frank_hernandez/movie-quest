package com.codeminion.moviequest.search.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.NestedScrollView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codeminion.moviequest.MovieResource
import com.codeminion.moviequest.R
import com.codeminion.moviequest.databinding.FragmentMovieDetailsBinding
import com.codeminion.moviequest.search.viewmodel.MovieDetailsViewModel
import com.codeminion.moviequest.search.viewmodel.SearchViewModel
import com.codeminion.omdbbackend.model.MovieBasicInfo
import com.codeminion.omdbbackend.model.MovieFullInfo
import kotlinx.android.synthetic.main.fragment_movie_details.*

class MovieDetailsFragment : Fragment() {

    private lateinit var mBinding:FragmentMovieDetailsBinding
    private lateinit var mMovieDetailsModel: MovieDetailsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_movie_details, container, false)

        activity?.let {
            mMovieDetailsModel = ViewModelProviders.of(it).get(MovieDetailsViewModel::class.java)

            val movieInfoData = mMovieDetailsModel.getMovieInfoData()
            movieInfoData.observe(this, Observer { resource ->
                handleMovieInfo(resource)
            })

            val movieSelected: SearchViewModel = ViewModelProviders.of(it).get(SearchViewModel::class.java)
            movieSelected.getSelectedMovieData().observe(this, Observer {movie->
                handleSelectedMovie(movie)
            })


            handleMovieInfo(movieInfoData.value)
            handleSelectedMovie(movieSelected.getSelectedMovieData().value)

            mBinding.itemDetails = mMovieDetailsModel
        }
        return mBinding.root
    }

    /**
     * Helper function to handle the data of the movie info.
     */
    private fun handleMovieInfo(movieInfo: MovieResource<MovieFullInfo>?) {
        movieInfo?.let {
            when(it.status) {
                MovieResource.SEARCH_DONE-> displayMovieData(it.data)
                MovieResource.SEARCH_ERROR -> activity?.finish()
                else -> it
            }
        }
    }

    /**
     * Helper function to handle displaying the movie data.
     */
    private fun displayMovieData(movieData: MovieFullInfo) {
        // Scroll to top so we can see the poster first.
        movieDetailsContainer?.scrollToStart()
        appbar?.setExpanded(true)
        mBinding.movieData = movieData
    }

    /**
     * Extension function for improved readability of the code.
     */
    private fun NestedScrollView.scrollToStart() {
        this.scrollTo(0,0)
    }

    /**
     * Helper function for handling the selected movie.
     */
    private fun handleSelectedMovie(movie:MovieBasicInfo?) {
        movie?.let {
            mMovieDetailsModel.updatePosterImage(it.posterUrlStr)
            // Query Movie Data
            mMovieDetailsModel.queryMovieInfoData(it.imdbID)
        }
    }
}