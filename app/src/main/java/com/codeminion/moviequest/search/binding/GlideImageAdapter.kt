package com.codeminion.moviequest.search.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.codeminion.moviequest.R

/**
 * Binding adapter to leverage Glide to load the images.
 */

object GlideImageAdapter {

    private val mGlideOptions: RequestOptions = RequestOptions()
        .error(R.drawable.ic_movie_quest_logo)

    @JvmStatic
    @BindingAdapter("imageUrl")
    fun glideLoadImage(view: ImageView, imageUrl: String?) {
        imageUrl?.let {
            Glide.with(view).load(imageUrl).apply(mGlideOptions).into(view)
        } ?: Glide.with(view).load("").apply(mGlideOptions).into(view)
    }
}
