package com.codeminion.moviequest.search.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.codeminion.moviequest.R
import com.codeminion.omdbbackend.model.MovieBasicInfo
import kotlinx.android.synthetic.main.list_item_movie.view.*

class MovieAdapter : RecyclerView.Adapter<MovieAdapter.MovieHolder>(), View.OnClickListener {

    private var mMovieItems: MutableList<MovieBasicInfo> = ArrayList()
    private var mClickListener:View.OnClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.list_item_movie, parent, false)
        return MovieHolder(view, this)
    }

    /**
     * Updates the list of items to display.
     */
    fun setItems(items: List<MovieBasicInfo>) {
        mMovieItems.clear()
        addItems(items)
    }

    /**
     * Appends the result to the existing list of
     * items.
     */
    fun addItems(items: List<MovieBasicInfo>) {
        mMovieItems.addAll(items)
    }

    override fun getItemCount(): Int = mMovieItems.size

    override fun onBindViewHolder(holder: MovieHolder, position: Int) = holder.bind(mMovieItems[position])

    override fun onClick(v: View?) {
        mClickListener?.let {
            it.onClick(v)
        }
    }

    /**
     * Sets a listener to be triggered when an item is clicked.
     */
    fun setOnItemClickListener(listener:View.OnClickListener ) {
        mClickListener = listener
    }


    class MovieHolder(view: View, clickListener:View.OnClickListener) : RecyclerView.ViewHolder(view) {

        private val mPoster: ImageView = view.moviePoster
        private val mTitle: TextView = view.movieTitle
        private val mYear: TextView = view.movieYear
        private val mClickListener:View.OnClickListener = clickListener

        companion object {
            private val mGlideOptions: RequestOptions = RequestOptions()
                .error(R.drawable.splash_bg)
                //.placeholder(R.mipmap.ic_launcher)
        }

        /**
         * Bind method to hide the internals of the view holder
         * and to maintain single responsibility.
         */
        fun bind(movie: MovieBasicInfo) {

            Glide.with(mPoster).load(movie.posterUrlStr).apply(mGlideOptions).into(mPoster)
            mTitle.text = movie.title
            mYear.text = movie.year
            itemView.setTag(R.id.itemData, movie)
            itemView.setOnClickListener(mClickListener)
        }
    }

}