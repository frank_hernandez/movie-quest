package com.codeminion.moviequest.search.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codeminion.moviequest.MovieResource
import com.codeminion.moviequest.R
import com.codeminion.moviequest.search.viewmodel.SearchViewModel
import com.codeminion.moviequest.search.adapter.MovieAdapter
import com.codeminion.omdbbackend.model.MovieBasicInfo
import kotlinx.android.synthetic.main.fragment_movie_list.*
import kotlinx.android.synthetic.main.fragment_movie_list.view.*

class MovieListFragment : Fragment(), View.OnClickListener {

    companion object {
        const val SCROLL_DIR_TO_END = 1
    }

    private lateinit var mAdapter: MovieAdapter
    private lateinit var mSearchModel: SearchViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movie_list, container, false).apply {
            val recyclerView: RecyclerView = movieList
            mAdapter = MovieAdapter()
            mAdapter.setOnItemClickListener(this@MovieListFragment)
            recyclerView.layoutManager = GridLayoutManager(activity, 3)
            recyclerView.adapter = mAdapter
            recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    // If we reached the bottom try to fetch the next results if any.
                    if (!recyclerView.canScrollVertically(SCROLL_DIR_TO_END)) {
                        mSearchModel.fetchNextTitlesPage()
                    }
                }
            })
            activity?.let {
                mSearchModel = ViewModelProviders.of(it).get(SearchViewModel::class.java)
                mSearchModel.getSearchModel().observe(this@MovieListFragment, Observer { resource ->
                    handleSearchModel(resource)
                })

                handleSearchModel(mSearchModel.getSearchModel().value)
            }
        }
    }

    private fun handleSearchModel(searchInfo: MovieResource<List<MovieBasicInfo>>?) {
        searchInfo?.let {
            if (it.status == MovieResource.SEARCH_DONE) {
                mAdapter.setItems(it.data)
                mAdapter.notifyDataSetChanged()

                if (it.data.isNotEmpty()) {
                    noMoviesMsg?.visibility = View.GONE
                }
            }
        }
    }

    override fun onClick(v: View) {
        v.getTag(R.id.itemData)?.let {
            val selectedMovie = it as MovieBasicInfo
            mSearchModel.getSelectedMovieData().value?.let { lastSelection ->
                // Only update the selection if a new one is made to not
                // update the UI unnecessarily.
                if (lastSelection != selectedMovie) {
                    mSearchModel.setSelectedMovie(selectedMovie)
                }
            } ?: mSearchModel.setSelectedMovie(selectedMovie)

        }
    }


}