package com.codeminion.moviequest.search

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codeminion.moviequest.MovieResource
import com.codeminion.moviequest.R
import com.codeminion.moviequest.search.viewmodel.SearchViewModel
import com.codeminion.omdbbackend.model.MovieBasicInfo
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_movie_list.*

class SearchActivity : AppCompatActivity() {

    //Note: I follow this convention but I am willing to switch to the team's standards
    private lateinit var mSearchModel: SearchViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.MovieQuest)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_list)

        mSearchModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)
        mSearchModel.getSearchModel().observe(this, Observer {
            handleSearchModel(it)
        })

        val selectedMovie = mSearchModel.getSelectedMovieData()
        selectedMovie.observe(this, Observer {
            handleSelectedMovie(it)
        })

        // Update the UI based on the current state of the models.
        handleSearchModel(mSearchModel.getSearchModel().value)
        handleSelectedMovie(selectedMovie.value)

        handleIntent(intent)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        handleIntent(intent)
    }

    /**
     * Helper method to centralize the handling of the
     * view model values for the search model.
     */
    private fun handleSearchModel(movieResource: MovieResource<List<MovieBasicInfo>>?) {
        movieResource?.let {
            when (it.status) {
                MovieResource.SEARCH_DONE,
                MovieResource.NONE -> searchProgress?.visibility = View.GONE
                MovieResource.SEARCH_ERROR -> {
                    searchProgress?.visibility = View.GONE
                    Snackbar.make(coordinatorLayout, "${it.error.message}", Snackbar.LENGTH_LONG).show()
                    mSearchModel.resetError()
                }
                else -> searchProgress?.visibility = View.VISIBLE
            }
        } ?: run { searchProgress?.visibility = View.GONE }
    }

    /**
     * Helper method to handle the selected movie.
     * When in landscape both fragments wil share the view model
     * so we only handle the case of small screens where we need
     * to launch a new activity to view the details.
     */
    private fun handleSelectedMovie(movieBasicInfo: MovieBasicInfo?) {
        movieBasicInfo?.let {
            if (movieDetailsFragment == null) {
                // Start Movie Details Activity
                val intent = Intent(this, MovieDetailsActivity::class.java)
                intent.putExtra(MovieDetailsActivity.EXTRA_SELECTED_MOVIE, movieBasicInfo)
                startActivity(intent)
                mSearchModel.setSelectedMovie(null)
            }
        }
    }

    /**
     * Helper method for handling all intents
     * in the activity.
     */
    private fun handleIntent(intent: Intent) {
        if (Intent.ACTION_SEARCH == intent.action) {
            val query = intent.getStringExtra(SearchManager.QUERY)
            searchMovieByTitle(query)
        }
    }

    /**
     * Helper method for requesting the list of
     * movies.
     */
    private fun searchMovieByTitle(titleQuery: String) {
        mSearchModel.findMoviesByTitle(titleQuery)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.search_activity_menu, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu.findItem(R.id.movieSearch)?.actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            maxWidth = Int.MAX_VALUE
        }

        return true
    }


}