package com.codeminion.moviequest.search.binding

import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.BindingAdapter
import com.codeminion.moviequest.R
import kotlinx.android.synthetic.main.item_actor.view.*
import kotlinx.android.synthetic.main.item_award.view.*

@BindingAdapter("actorsList")
fun LinearLayout.listActors(actors:String) {

    this.removeAllViews()
    val awardsArr = actors.split(",")

    val inflater = LayoutInflater.from(this.context)

    awardsArr.filter { it.isNotEmpty() }.forEach {
        val itemRow = inflater.inflate(R.layout.item_actor, this, false)
        itemRow.actorTitle.text = it.trim()
        this.addView(itemRow)
    }
}

@BindingAdapter("awardsList")
fun LinearLayout.listAwards(awards:String) {

    this.removeAllViews()
    val awardsArr = awards.split(".")

    val inflater = LayoutInflater.from(this.context)

    awardsArr.filter { it.isNotEmpty() }.forEach {
        val itemRow = inflater.inflate(R.layout.item_award, this, false)
        itemRow.awardTitle.text = it.trim()
        this.addView(itemRow)
    }
}