package com.codeminion.moviequest.search.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codeminion.moviequest.MovieRepository
import com.codeminion.moviequest.MovieResource
import com.codeminion.moviequest.RepositoryBridge
import com.codeminion.omdbbackend.model.MovieFullInfo
import org.jetbrains.anko.doAsync
import java.lang.Exception

/**
 * View model for the item details UI
 */
class MovieDetailsViewModel : ViewModel() {

    private val mRepo:MovieRepository = RepositoryBridge.getRepository()

    private val mImageUrl:ObservableField<String> = ObservableField()

    private val mMovieDetailsData:MutableLiveData<MovieResource<MovieFullInfo>> = MutableLiveData()

    /**
     * @return Returns the observable image URL for the poster
     */
    fun getPosterImageUrl() = mImageUrl

    /**
     * Updates the existing poster URL
     */
    fun updatePosterImage(imageUrl:String) {
        mImageUrl.set(imageUrl)
    }

    /**
     * Returns the data for the Full movie info.
     */
    fun getMovieInfoData() = mMovieDetailsData

    /**
     * Retrieves the Movie information for the given movie/show id.
     */
    fun queryMovieInfoData(movieId:String):LiveData<MovieResource<MovieFullInfo>> {

        mMovieDetailsData.value = MovieResource<MovieFullInfo>(MovieResource.SEARCHING, null)

        doAsync {
            try {
                val movieInfo = mRepo.findMovieById(movieId)
                mMovieDetailsData.postValue(MovieResource(MovieResource.SEARCH_DONE, movieInfo))
            } catch (e:Exception) {
                mMovieDetailsData.postValue(MovieResource<MovieFullInfo>(MovieResource.SEARCH_ERROR, null, e))
            }
        }

        return mMovieDetailsData
    }

}