package com.codeminion.moviequest.search

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codeminion.moviequest.MovieResource
import com.codeminion.moviequest.R
import com.codeminion.moviequest.search.viewmodel.MovieDetailsViewModel
import com.codeminion.moviequest.search.viewmodel.SearchViewModel
import com.codeminion.omdbbackend.model.MovieBasicInfo
import com.codeminion.omdbbackend.model.MovieFullInfo
import kotlinx.android.synthetic.main.fragment_movie_details.*
import kotlinx.android.synthetic.main.layout_app_bar.*
import kotlinx.android.synthetic.main.layout_progress.*

class MovieDetailsActivity : AppCompatActivity() {

    companion object{
        const val EXTRA_SELECTED_MOVIE = "com.codeminion.moviequest.search.extras.EXTRA_SELECTED_MOVIE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)

        this.setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val selectedMovie: MovieBasicInfo = intent.getParcelableExtra(EXTRA_SELECTED_MOVIE)
        collapsingToolbar.title = selectedMovie.title

        val searchViewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)
        searchViewModel.setSelectedMovie(selectedMovie)

        val movieDetailsModel = ViewModelProviders.of(this).get(MovieDetailsViewModel::class.java)

        val movieInfoData = movieDetailsModel.getMovieInfoData()
        movieInfoData.observe(this, Observer {
            handleMovieInfo(it)
        })

        handleMovieInfo(movieInfoData.value)

    }

    /**
     * Helper function to handle the data of the movie info.
     */
    private fun handleMovieInfo(movieInfo: MovieResource<MovieFullInfo>?) {

        movieInfo?.let {
            when(it.status) {
                MovieResource.SEARCHING -> searchProgress.visibility = View.VISIBLE
                else -> searchProgress.visibility = View.GONE
            }
        }

    }
}