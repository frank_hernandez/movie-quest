package com.codeminion.moviequest.search.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codeminion.moviequest.MovieRepository
import com.codeminion.moviequest.MovieResource
import com.codeminion.moviequest.RepositoryBridge
import com.codeminion.omdbbackend.model.MovieBasicInfo
import org.jetbrains.anko.doAsync
import java.lang.Exception

/**
 * View model for the Search activity.
 */
class SearchViewModel : ViewModel() {

    private var mCurrentPage:Long = 1
    private var mResults:MutableList<MovieBasicInfo> = arrayListOf()
    private var mCurrentResultsTotal:Long = 0
    private var mLastTitleQuery:String = ""

    private val mRepository: MovieRepository = RepositoryBridge.getRepository()
    private val mSelectedMovieData: MutableLiveData<MovieBasicInfo> = MutableLiveData()
    private val mSearchLiveData: MutableLiveData<MovieResource<List<MovieBasicInfo>>> = MutableLiveData()

    init {
        mSearchLiveData.value = MovieResource(MovieResource.NONE, listOf())
    }

    /**
     * Returns the search data for the UI.
     */
    fun getSearchModel(): LiveData<MovieResource<List<MovieBasicInfo>>> = mSearchLiveData

    /**
     * Requests a search to be performed with the given title
     */
    fun findMoviesByTitle(titleQuery: String) {

        mResults.clear()
        mCurrentPage = 0
        mCurrentResultsTotal = 0

        findMoviesByTitle(titleQuery, mCurrentPage+1)
    }

    /**
     * Fetches the next page if any are available.
     */
    fun fetchNextTitlesPage(){
        if(mResults.size < mCurrentResultsTotal && mLastTitleQuery.isNotEmpty()) {
            findMoviesByTitle(mLastTitleQuery, mCurrentPage+1)
        }
    }

    /**
     * Helper function to query for movies with by title
     * @param titleQuery title query to use
     * @param page result page to show
     */
    private fun findMoviesByTitle(titleQuery: String, page:Long) {
        val query = titleQuery.trim()
        mLastTitleQuery = query
        val status = MovieResource.SEARCHING
        mSearchLiveData.postValue(MovieResource(status, listOf()))

        doAsync {
            // Call repo layer for data.
            try {
                mRepository.findMoviesByTitle(query, page).apply {
                    mResults.addAll(results)
                    mCurrentPage +=1
                    mCurrentResultsTotal = resultTotal
                    mSearchLiveData.postValue(MovieResource(MovieResource.SEARCH_DONE, mResults))
                }

            } catch (e: Exception) {
                mSearchLiveData.postValue(MovieResource(MovieResource.SEARCH_ERROR, mResults, e))
            }
        }

        mResults.filter { true }
    }

    /**
     * Resets the error state for the view.
     */
    fun resetError() = mSearchLiveData.postValue(MovieResource(MovieResource.SEARCH_DONE, mResults))


    /**
     * Returns the data about the selected movie.
     */
    fun getSelectedMovieData():LiveData<MovieBasicInfo> = mSelectedMovieData

    /**
     * Set the selected movie.
     */
    fun setSelectedMovie(movieData:MovieBasicInfo?) {
        mSelectedMovieData.value=movieData
    }

}