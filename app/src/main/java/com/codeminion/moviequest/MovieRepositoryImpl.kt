package com.codeminion.moviequest

import com.codeminion.moviequest.exception.SearchFailedException
import com.codeminion.moviequest.model.ResultPage
import com.codeminion.omdbbackend.OmdbBackendApi
import com.codeminion.omdbbackend.model.MovieBasicInfo
import com.codeminion.omdbbackend.model.MovieFullInfo

class MovieRepositoryImpl : MovieRepository {

    companion object {
        private const val API_KEY = "7e9632f1"
    }

    override fun findMoviesByTitle(titleStr: String, page: Long): ResultPage<MovieBasicInfo> {
        val response = OmdbBackendApi.getOmdbService().findMoviesByTitle(API_KEY, titleStr, page).execute()

        if (response.isSuccessful) {
            response.body()?.let {
                return when (it.isResponse()) {
                    true -> ResultPage(
                        results = it.searchResults,
                        resultTotal = it.totalResults,
                        pageNum = page
                    )
                    else -> throw SearchFailedException(msg = it.errorMsg)
                }
            }
        }

        throw SearchFailedException()
    }

    override fun findMovieById(id: String): MovieFullInfo {
        val response = OmdbBackendApi.getOmdbService().findMovieById(
            apiKey = API_KEY,
            movieId = id
        ).execute()

        if (response.isSuccessful) {
            response.body()?.let {
                return when (it.isResponse()) {
                    true -> it
                    false -> throw SearchFailedException(msg = it.errorMsg)
                }
            }

        }

        throw SearchFailedException()
    }

}

