package com.codeminion.moviequest

object RepositoryBridge {

    private lateinit var mMovieRepository: MovieRepository

    /**
     * Factory method for replacing
     * repositories as needed.
     * Returns an instance of the repository to use.
     */
    fun getRepository():MovieRepository {

        if(!::mMovieRepository.isInitialized) {
            mMovieRepository = MovieRepositoryImpl()
        }

        return mMovieRepository
    }

    /**
     * Sets the current repo to use
     */
    fun setRepositoryImpl(repo:MovieRepository) {
        mMovieRepository = repo
    }
}