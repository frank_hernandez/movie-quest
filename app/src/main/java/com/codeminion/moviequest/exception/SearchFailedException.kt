package com.codeminion.moviequest.exception

import java.lang.Exception

data class SearchFailedException(val msg:String = ""):Exception(msg)