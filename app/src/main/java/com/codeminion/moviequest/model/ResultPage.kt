package com.codeminion.moviequest.model


/**
 * Represents a single page of results
 */
data class ResultPage<T>(
    val results: List<T>,
    val resultTotal: Long, // Total available records
    val pageNum: Long
)